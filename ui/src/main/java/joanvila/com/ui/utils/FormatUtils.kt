package joanvila.com.ui.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class FormatUtils {



    companion object {
        val API_FORMAT = "yyyy-MM-dd HH:mm:ss"
        val DISPLAY_SIMPLE_FORMAT = "dd-MM-yyyy"
        val DISPLAY_HOUR_FORMAT = "HH:mm"

        private val apiFormat = SimpleDateFormat(API_FORMAT, Locale.getDefault())
        private val ddMmYyyyFormat = SimpleDateFormat(DISPLAY_SIMPLE_FORMAT, Locale
                .getDefault())

        private val hourFormat = SimpleDateFormat(DISPLAY_HOUR_FORMAT, Locale.getDefault())

        fun parseApiFormat(date: String): Date {
            return apiFormat.parse(date)
        }

        fun parseDefault(date: String): Date {
            return apiFormat.parse(date)
        }

        fun displayDate(date: Date) : String {
            return ddMmYyyyFormat.format(date)
        }

        fun displayDate(date: String, format: SimpleDateFormat = apiFormat): String {
            return displayDate(format.parse(date))
        }

        fun getHoursText(date: Date) : String {
            return hourFormat.format(date)
        }

        fun format(date: Date): String {
            return apiFormat.format(date)
        }

        fun getTotalHoursText(totalHoursMillis: Long): String {
            return String.format("%d h %d m",
                    TimeUnit.MILLISECONDS.toHours(totalHoursMillis),
                    TimeUnit.MILLISECONDS.toMinutes(totalHoursMillis) - TimeUnit.HOURS.toMinutes
                    (TimeUnit.MILLISECONDS.toHours(totalHoursMillis))
            )
        }

        fun toHours(millis: Long) : Double {
            return (millis.toDouble() / (1000*60*60))
        }

        fun updateHourAndMinutes(referenceDate: Date, hourText: String) : Date {
            val cal = Calendar.getInstance()
            cal.time = referenceDate
            cal.set(cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DATE),
                    FormatUtils.parseHour(hourText),
                    FormatUtils.parseMinute(hourText))
            return cal.time
        }

        fun parseHour(hourText: String) : Int {
            val cal = Calendar.getInstance()
            cal.time = hourFormat.parse(hourText)
            return cal.get(Calendar.HOUR_OF_DAY)
        }

        fun parseMinute(hourText: String) : Int {
            val cal = Calendar.getInstance()
            cal.time = hourFormat.parse(hourText)
            return cal.get(Calendar.MINUTE)
        }

        fun parseDate(date: Date, simpleDataFormat: SimpleDateFormat) :String =
                simpleDataFormat.format(date).toString()

    }



}