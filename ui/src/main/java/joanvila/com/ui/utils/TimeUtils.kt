package joanvila.com.ui.utils

import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ceil

class TimeUtils {
    companion object {
        val DEFAULT_DATE_FORMATTER = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    }
}