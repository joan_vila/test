package joanvila.com.ui.utils

import android.content.Context
import joanvila.com.data.model.NetworkException
import joanvila.com.ui.R

fun Throwable.extractErrorMessage(context: Context) = when (this) {
    is NetworkException.UnauthorizedException -> context.getString(R.string.unauthorized_error)
    is NetworkException.NoInternetConnection -> context.getString(R.string.no_internet_exception)
    is NetworkException.ServerException -> context.getString(R.string.server_error)
    is NetworkException.UnprocessableEntityException -> error
    else -> message ?: context.getString(R.string.unknown_error)
}