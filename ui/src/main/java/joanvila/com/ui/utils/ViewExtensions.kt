package joanvila.com.ui.utils

import android.app.DatePickerDialog
import android.content.Context
import android.os.SystemClock
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import joanvila.com.ui.adapter.GenericAdapter
import java.util.*

/**
 * Sets a view visibility to [View.VISIBLE]
 */
fun View.setVisible() {
    this.visibility = View.VISIBLE
}

/**
 * Sets a view visibility to [View.INVISIBLE]
 */

fun View.setInvisible() {
    this.visibility = View.INVISIBLE
}

/**
 * Sets a view visibility to [View.GONE]
 */
fun View.setGone() {
    this.visibility = View.GONE
}


fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

fun View.isInvisible(): Boolean {
    return this.visibility == View.INVISIBLE
}

fun View.isGone(): Boolean {
    return this.visibility == View.GONE
}

fun View.onClick(interval: Long = 1000, action: (View) -> Unit) {
    val lastClickMap = WeakHashMap<View, Long>()

    setOnClickListener {
        val previousClickTimestamp = lastClickMap[it]
        val currentTimestamp = SystemClock.uptimeMillis()
        lastClickMap[it] = currentTimestamp

        if( previousClickTimestamp == null ||
            Math.abs(currentTimestamp - previousClickTimestamp) > interval) {
                action(this)
        }
    }
}

fun RecyclerView.setupWithEndless(adapter: GenericAdapter,
                                 layoutManager: RecyclerView.LayoutManager,
                                 itemDecoration: RecyclerView.ItemDecoration? = null,
                                 loadMore: (Int, Int) -> Unit): EndlessRecyclerOnScrollListener {

    this.layoutManager = layoutManager
    this.adapter = adapter

    itemDecoration?.let { this.addItemDecoration(it) }
    val customScrollListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
        override fun onLoadMore(page: Int, totalItemsCount: Int) {
            loadMore(page, totalItemsCount)
        }
    }

    this.clearOnScrollListeners()
    this.addOnScrollListener(customScrollListener)

    return customScrollListener
}

fun showDateDialog(context: Context, currentDate: Calendar = Calendar.getInstance(), onDateSelected: (Date) -> Unit) {

    val dialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month,
                                                                                dayOfMonth ->
        val selectedDate = Calendar.getInstance()
        selectedDate.set(year, month, dayOfMonth)
        onDateSelected(selectedDate.time)
    }
            , currentDate.get(Calendar.YEAR)
            , currentDate.get(Calendar.MONTH)
            , currentDate.get(Calendar.DAY_OF_MONTH))


    dialog.show()
}


