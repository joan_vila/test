package joanvila.com.ui.navigation

import androidx.appcompat.app.AppCompatActivity
import joanvila.com.presentation.airports.AirportViewEntity
import joanvila.com.presentation.schedules.ScheduleViewEntity
import joanvila.com.ui.MainActivity
import joanvila.com.ui.airports.AirportsActivity
import joanvila.com.ui.routes.RoutesActivity
import joanvila.com.ui.schedules.SchedulesActivity

class Navigator(private val context: AppCompatActivity) {

    fun navigateToSearchAirports(title: String, reqCode: Int) {
        context.startActivityForResult(AirportsActivity.getIntent(title, context), reqCode)
    }

    fun navigateToMainMenu() {
        context.startActivity(MainActivity.getIntent(context))
    }

    fun navigateToSchedules(origin: AirportViewEntity, destination: AirportViewEntity) {
        context.startActivity(SchedulesActivity.getIntent(origin, destination, context))
    }

    fun navigateToRoutes(schedule: ScheduleViewEntity) {
        context.startActivity(RoutesActivity.getIntent(schedule, context))
    }
}