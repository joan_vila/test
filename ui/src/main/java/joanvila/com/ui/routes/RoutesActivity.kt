package joanvila.com.ui.routes

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.routes.RoutesIntent
import joanvila.com.presentation.routes.RoutesUiModel
import joanvila.com.presentation.routes.RoutesViewModel
import joanvila.com.presentation.schedules.ScheduleViewEntity
import joanvila.com.ui.R
import joanvila.com.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_routes.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RoutesActivity : BaseActivity<RoutesIntent, RoutesUiModel>(), OnMapReadyCallback {

    private val viewModel by viewModel<RoutesViewModel>()
    private var disposables = CompositeDisposable()
    private lateinit var schedule: ScheduleViewEntity
    private val onMapLoaded: PublishSubject<RoutesIntent.OnMapaLoaded> = PublishSubject.create()
    private var map: GoogleMap? = null

    companion object {

        const val EXTRA_SCHEDULE = "SCHEDULE"

        @JvmStatic
        fun getIntent(schedule: ScheduleViewEntity,  context: Context): Intent {
            val intent = Intent(context, RoutesActivity::class.java)
            intent.putExtra(EXTRA_SCHEDULE, schedule)
            return intent
        }
    }

    override var layout = R.layout.activity_routes

    override fun intents(): Observable<RoutesIntent> {
        return Observable.merge<RoutesIntent>(listOf(initialIntent(), onMapLoaded))
    }

    private fun initialIntent(): Observable<RoutesIntent.InitialIntent> {
        return Observable.just(RoutesIntent.InitialIntent)
    }

    override fun onViewLoaded(savedInstanceState: Bundle?) {
        schedule = intent.getParcelableExtra(EXTRA_SCHEDULE)
        configViews()
        disposables.add(viewModel.states().subscribe(this::render))
        viewModel.processIntents(intents())
    }

    override fun render(state: RoutesUiModel) {
        when(state) {
            is RoutesUiModel.Success -> {
                progress_bar.hide()
                val points = state.data?.map {
                    LatLng(it.first.toDouble(), it.second.toDouble())
                }?.toTypedArray()
                drawPolyline(points)
            }
            is RoutesUiModel.InProgress -> {
                progress_bar.show()
            }
            is RoutesUiModel.Failed -> {
                progress_bar.hide()
            }
        }
    }

    private fun drawPolyline(points: Array<LatLng>?) {
        points?.let {
            val polyline = PolylineOptions()
                    .add(*it)
                    .width(5f)
                    .color(Color.RED)

            map?.addPolyline(polyline)
        }
    }
    private fun configViews() {
        // Config action bar
        supportActionBar?.let { actionBar ->
            actionBar.title = getString(R.string.routes_title)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        // Init map view
        val supportMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        supportMapFragment?.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        onMapLoaded.onNext(RoutesIntent.OnMapaLoaded(schedule))
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

}
