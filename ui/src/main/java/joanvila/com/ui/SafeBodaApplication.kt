package joanvila.com.ui

import android.app.Application
import com.evernote.android.state.StateSaver
import joanvila.com.data.di.dataModule
import joanvila.com.presentation.di.presentationModule
import joanvila.com.ui.di.uiModule
import org.koin.android.ext.android.startKoin

class SafeBodaApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(uiModule, presentationModule, dataModule))
        StateSaver.setEnabledForAllActivitiesAndSupportFragments(this, true);
    }

}