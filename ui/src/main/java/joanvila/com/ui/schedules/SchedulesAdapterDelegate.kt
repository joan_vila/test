package joanvila.com.ui.schedules

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import joanvila.com.presentation.schedules.ScheduleViewEntity
import joanvila.com.presentation.utils.ViewType
import joanvila.com.presentation.utils.ViewTypeConstants
import joanvila.com.ui.R
import joanvila.com.ui.adapter.AdapterDelegate
import joanvila.com.ui.adapter.BaseListener
import joanvila.com.ui.adapter.ItemViewHolder
import kotlinx.android.synthetic.main.item_schedule.view.*

class SchedulesAdapterDelegate: AdapterDelegate<List<ViewType>>() {

    interface SchedulesClickListener: BaseListener {
        fun onScheduleClicked(schedule: ScheduleViewEntity)
    }

    override var viewTypeId = ViewTypeConstants.VIEW_TYPE_SCHEDULE

    override fun isForViewType(items: List<ViewType>, position: Int) = items[position] is ScheduleViewEntity


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_schedule, parent, false)
        return ScheduleViewHolder(view, listener, context)
    }

    override fun onBindViewHolder(items: List<ViewType>, position: Int, holder: RecyclerView.ViewHolder, payloads: List<Any>) {
        (holder as? ScheduleViewHolder)?.bind(items[position] as ScheduleViewEntity)
    }

    class ScheduleViewHolder(view: View, val listener: BaseListener?,
                            val context: Context) : ItemViewHolder(view) {
        fun bind(schedule: ScheduleViewEntity) {
            val date = schedule.flights.first().arrival.dateTime
            itemView.tv_departure_date.text = date
            itemView.tv_duration.text = "Duration: ${schedule.duration}"
            itemView.tv_flights_count.text = "Flights count: ${schedule.flights.size}"
            itemView.setOnClickListener {
                listener?.let { (it as SchedulesClickListener).onScheduleClicked(schedule) }
            }
        }

    }

}