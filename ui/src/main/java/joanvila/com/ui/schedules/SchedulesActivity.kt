package joanvila.com.ui.schedules

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.airports.AirportViewEntity
import joanvila.com.presentation.schedules.ScheduleViewEntity
import joanvila.com.presentation.schedules.SchedulesIntent
import joanvila.com.presentation.schedules.SchedulesUiModel
import joanvila.com.presentation.schedules.SchedulesViewModel
import joanvila.com.presentation.utils.ViewType
import joanvila.com.ui.R
import joanvila.com.ui.adapter.AdapterDelegate
import joanvila.com.ui.adapter.GenericAdapter
import joanvila.com.ui.adapter.LoadingAdapterDelegate
import joanvila.com.ui.base.BaseActivity
import joanvila.com.ui.utils.*
import kotlinx.android.synthetic.main.activity_schedules.*
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class SchedulesActivity: BaseActivity<SchedulesIntent, SchedulesUiModel>(),
        SchedulesAdapterDelegate.SchedulesClickListener{

    private val viewModel by viewModel<SchedulesViewModel>()
    private var disposables = CompositeDisposable()
    private lateinit var adapter: GenericAdapter
    private var scrollListener: EndlessRecyclerOnScrollListener? = null
    private val loadMore: PublishSubject<SchedulesIntent.OnLoadMore> = PublishSubject.create()
    private val selectDate: PublishSubject<SchedulesIntent.SelectDate> = PublishSubject.create()
    private var origin: AirportViewEntity? = null
    private var destination: AirportViewEntity? = null
    companion object {

        const val EXTRA_ORIGIN = "ORIGIN"
        const val EXTRA_DESTINATION = "DESTINATION"

        @JvmStatic
        fun getIntent(origin: AirportViewEntity, destination: AirportViewEntity, context: Context): Intent {
            val intent = Intent(context, SchedulesActivity::class.java)
            intent.putExtra(EXTRA_ORIGIN, origin)
            intent.putExtra(EXTRA_DESTINATION, destination)
            return intent
        }
    }

    override var layout = R.layout.activity_schedules

    override fun onViewLoaded(savedInstanceState: Bundle?) {

        origin = intent.getParcelableExtra(EXTRA_ORIGIN)
        destination = intent.getParcelableExtra(EXTRA_DESTINATION)

        configViews()
        setupListeners()
        disposables.add(viewModel.states().subscribe(this::render))
        viewModel.processIntents(intents())
    }

    override fun intents(): Observable<SchedulesIntent> {
        return Observable.merge<SchedulesIntent>(listOf(initialIntent(), loadMore, selectDate))
    }

    private fun initialIntent(): Observable<SchedulesIntent.InitialIntent> {
        return Observable.just(SchedulesIntent
                .InitialIntent(
                        btn_date_selector.text.toString(),
                        origin?.id ?: "",
                        destination?.id ?: ""
                ))
    }

    private fun configViews() {

        // Config action bar
        supportActionBar?.let { actionBar ->
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        // Config recycler view
        val delegates = LinkedHashSet<AdapterDelegate<List<ViewType>>>()
        val loadingAdapterDelegate: LoadingAdapterDelegate = get()
        val schedulesAdapterDelegate: SchedulesAdapterDelegate = get()
        delegates.add(loadingAdapterDelegate)
        delegates.add(schedulesAdapterDelegate)
        adapter = GenericAdapter(delegates)
        adapter.setClickListener(this)
        scrollListener = rv_schedules.setupWithEndless(adapter, LinearLayoutManager(this), null,
                loadMore = { _, total ->
                loadMore.onNext(
                        SchedulesIntent.OnLoadMore(
                                btn_date_selector.text.toString(),
                                origin?.id ?: "",
                                destination?.id ?: "",
                                total
                        )
                )
        })
        val currentDate = Calendar.getInstance()
        btn_date_selector.text = FormatUtils.parseDate(currentDate.time, TimeUtils.DEFAULT_DATE_FORMATTER)
    }

    private fun setupListeners() {
        btn_date_selector.onClick {view ->
            createDatePicker(view as Button)
        }
    }

    private fun createDatePicker(button: Button) {
        val cal = Calendar.getInstance()

        showDateDialog(this) {
            cal.time = it
            val formattedDate = FormatUtils.parseDate(cal.time, TimeUtils.DEFAULT_DATE_FORMATTER)
            button.text = formattedDate
            selectDate.onNext(SchedulesIntent.SelectDate(
                    formattedDate,
                    origin?.id ?: "",
                    destination?.id ?: ""
            ))
        }
    }

    override fun render(state: SchedulesUiModel) {
        when(state) {
            is SchedulesUiModel.Success -> {
                state.data?.let {
                    if (state.loadMore) {
                        adapter.addAll(it)
                    } else {
                        adapter.set(newItems = it)
                        if (it.isEmpty()) {
                            tv_empty.setVisible()
                            rv_schedules.setGone()
                        } else {
                            tv_empty.setGone()
                            rv_schedules.setVisible()
                        }
                    }
                }
            }
            is SchedulesUiModel.InProgress -> {
                if (state.refresh) {
                    scrollListener?.resetState()
                    adapter.clear()
                }
                adapter.addLoadingView()
            }

            is SchedulesUiModel.Failed -> {
                adapter.removeLoadingView()
                showError(state.exception)
            }
        }
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    override fun onScheduleClicked(schedule: ScheduleViewEntity) {
        navigator.navigateToRoutes(schedule)
    }

}