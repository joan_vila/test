package joanvila.com.ui.di

import androidx.appcompat.app.AppCompatActivity
import domain.async.PostExecutionThread
import joanvila.com.ui.UiThread
import joanvila.com.ui.adapter.LoadingAdapterDelegate
import joanvila.com.ui.airports.AirportsAdapterDelegate
import joanvila.com.ui.navigation.Navigator
import joanvila.com.ui.schedules.SchedulesAdapterDelegate
import org.koin.dsl.module.module

val uiModule = module {
    factory { (activity: AppCompatActivity) -> Navigator(activity) }
    factory<PostExecutionThread> { UiThread() }
    factory { AirportsAdapterDelegate() }
    factory { SchedulesAdapterDelegate() }
    factory { LoadingAdapterDelegate() }
}