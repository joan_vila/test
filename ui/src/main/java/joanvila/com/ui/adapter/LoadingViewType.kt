package joanvila.com.ui.adapter

import android.os.Parcel
import android.os.Parcelable
import joanvila.com.presentation.utils.ViewType
import joanvila.com.presentation.utils.ViewTypeConstants

class LoadingViewType : ViewType {

    override fun getViewType() = ViewTypeConstants.VIEW_TYPE_LOADING

    override fun writeToParcel(parcel: Parcel, flags: Int) {}

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<LoadingViewType> {
        override fun createFromParcel(parcel: Parcel): LoadingViewType = LoadingViewType()
        override fun newArray(size: Int): Array<LoadingViewType?> = arrayOfNulls(size)
    }
}