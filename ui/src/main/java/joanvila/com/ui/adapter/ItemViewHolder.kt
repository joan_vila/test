package joanvila.com.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

open class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view)