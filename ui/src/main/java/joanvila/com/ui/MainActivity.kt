package joanvila.com.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.evernote.android.state.State
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import joanvila.com.presentation.airports.AirportViewEntity
import joanvila.com.presentation.base.BaseIntent
import joanvila.com.presentation.base.BaseViewState
import joanvila.com.ui.airports.AirportsActivity
import joanvila.com.ui.base.BaseActivity
import joanvila.com.ui.utils.onClick
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<BaseIntent, BaseViewState>() {

    private var disposables = CompositeDisposable()

    companion object {
        const val REQ_CODE_DEPARTURE = 1
        const val REQ_CODE_DESTINATION = 2

        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    @State
    var origin: AirportViewEntity? = null
    @State
    var arrival: AirportViewEntity? = null

    override var layout = R.layout.activity_main

    override fun onViewLoaded(savedInstanceState: Bundle?) {
        setupListeners()
    }

    private fun setupListeners() {
        et_origin.onClick {
            navigator.navigateToSearchAirports(getString(R.string.search_airports_departure_title), REQ_CODE_DEPARTURE)
        }
        et_arrival.onClick {
            navigator.navigateToSearchAirports( getString(R.string.search_airports_destination_title), REQ_CODE_DESTINATION)
        }
        val departureObservable = et_origin.textChanges()
        val destinationObservable = et_arrival.textChanges()

        disposables.add(Observables.combineLatest(departureObservable, destinationObservable) { origin, arrival ->
            val isEnabled = origin.isNotEmpty() && arrival.isNotEmpty()
            if (isEnabled) {
                btn_next_step.background.alpha = 255
            } else {
                btn_next_step.background.alpha = 128
            }
        }.subscribe())

        btn_next_step.onClick {
            if (origin != null && arrival != null) {
                navigator.navigateToSchedules(origin!!, arrival!!)
            } else {
                showError(Throwable("Must select an origin and a arrival point"))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val airport = data?.getParcelableExtra<AirportViewEntity>(AirportsActivity.EXTRA_AIRPORT)
            when (requestCode) {
                REQ_CODE_DEPARTURE -> {
                    origin = airport
                    et_origin.setText(origin?.name)
                }
                REQ_CODE_DESTINATION -> {
                    arrival = airport
                    et_arrival.setText(arrival?.name)
                }
            }
        }
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }
}
