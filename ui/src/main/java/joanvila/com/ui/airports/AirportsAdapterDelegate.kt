package joanvila.com.ui.airports

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import joanvila.com.presentation.airports.AirportViewEntity
import joanvila.com.presentation.utils.ViewType
import joanvila.com.presentation.utils.ViewTypeConstants
import joanvila.com.ui.R
import joanvila.com.ui.adapter.AdapterDelegate
import joanvila.com.ui.adapter.BaseListener
import joanvila.com.ui.adapter.ItemViewHolder
import kotlinx.android.synthetic.main.item_airport.view.*

class AirportsAdapterDelegate: AdapterDelegate<List<ViewType>>() {
    interface AirportsClickListener: BaseListener {
        fun onAirportClicked(airport: AirportViewEntity)
    }

    override var viewTypeId = ViewTypeConstants.VIEW_TYPE_AIRPORT

    override fun isForViewType(items: List<ViewType>, position: Int) = items[position] is AirportViewEntity


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_airport, parent, false)
        return AirportViewHolder(view, listener, context)
    }

    override fun onBindViewHolder(items: List<ViewType>, position: Int, holder: RecyclerView.ViewHolder, payloads: List<Any>) {
        (holder as? AirportViewHolder)?.bind(items[position] as AirportViewEntity)
    }

    class AirportViewHolder(view: View, val listener: BaseListener?,
                            val context: Context) : ItemViewHolder(view) {
        fun bind(airport: AirportViewEntity) {
            itemView.tv_id.text = airport.id
            itemView.tv_name.text = "City code: ${airport.name}"
            itemView.setOnClickListener {
                listener?.let { (it as AirportsClickListener).onAirportClicked(airport) }
            }
        }

    }

}