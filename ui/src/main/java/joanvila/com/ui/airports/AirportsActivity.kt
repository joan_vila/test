package joanvila.com.ui.airports

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.airports.AirportViewEntity
import joanvila.com.presentation.airports.AirportsIntent
import joanvila.com.presentation.airports.AirportsUiModel
import joanvila.com.presentation.airports.AirportsViewModel
import joanvila.com.presentation.utils.ViewType
import joanvila.com.ui.R
import joanvila.com.ui.adapter.AdapterDelegate
import joanvila.com.ui.adapter.GenericAdapter
import joanvila.com.ui.adapter.LoadingAdapterDelegate
import joanvila.com.ui.base.BaseActivity
import joanvila.com.ui.utils.*
import kotlinx.android.synthetic.main.activity_airports.*
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


class AirportsActivity : BaseActivity<AirportsIntent, AirportsUiModel>(),
        AirportsAdapterDelegate.AirportsClickListener {

    private val viewModel by viewModel<AirportsViewModel>()
    private var disposables = CompositeDisposable()
    private lateinit var adapter: GenericAdapter
    private var scrollListener: EndlessRecyclerOnScrollListener? = null
    private val loadMore: PublishSubject<AirportsIntent.OnLoadMore> = PublishSubject.create()

    companion object {

        const val EXTRA_AIRPORT = "AIRPORT"
        const val EXTRA_TITLE = "TITLE"

        @JvmStatic
        fun getIntent(title: String, context: Context): Intent {
            val intent = Intent(context, AirportsActivity::class.java)
            intent.putExtra(EXTRA_TITLE, title)
            return intent
        }
    }

    override var layout = R.layout.activity_airports

    override fun onViewLoaded(savedInstanceState: Bundle?) {
        configViews()
        disposables.add(viewModel.states().subscribe(this::render))
        viewModel.processIntents(intents())
    }

    override fun intents(): Observable<AirportsIntent> {
        return Observable.merge<AirportsIntent>(listOf(initialIntent(), searchIntent(), loadMore))
    }

    private fun initialIntent(): Observable<AirportsIntent.InitialIntent> {
        return Observable.just(AirportsIntent.InitialIntent)
    }

    private fun searchIntent(): Observable<AirportsIntent.EnterName> {
        return et_search.textChanges()
                .debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map {
                    AirportsIntent.EnterName(it.toString())
                }
    }

    private fun configViews() {

        // Config action bar
        supportActionBar?.let { actionBar ->
            intent.getStringExtra(EXTRA_TITLE)?.let { title ->
                actionBar.title = title
            }
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        // Config recycler view
        val delegates = LinkedHashSet<AdapterDelegate<List<ViewType>>>()
        val loadingAdapterDelegate: LoadingAdapterDelegate = get()
        val airportsAdapterDelegate: AirportsAdapterDelegate = get()
        delegates.add(loadingAdapterDelegate)
        delegates.add(airportsAdapterDelegate)
        adapter = GenericAdapter(delegates)
        adapter.setClickListener(this)
        scrollListener = rv_airports.setupWithEndless(
                adapter,
                LinearLayoutManager(this),
                null,
                loadMore = { _, total ->
                    loadMore.onNext(AirportsIntent.OnLoadMore(et_search.text.toString(), total))
                })
    }

    override fun render(state: AirportsUiModel) {
        when (state) {
            is AirportsUiModel.Success -> {
                state.data?.let {
                    if (state.loadMore) {
                        adapter.addAll(it)
                    } else {
                        scrollListener?.resetState()
                        adapter.set(newItems = it)
                        if (it.isEmpty()) {
                            tv_empty.setVisible()
                            rv_airports.setGone()
                        } else {
                            tv_empty.setGone()
                            rv_airports.setVisible()
                        }
                    }

                }
            }
            is AirportsUiModel.InProgress -> {
                adapter.addLoadingView()
            }
            is AirportsUiModel.Failed ->
                showError(state.exception)

        }
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    override fun onAirportClicked(airport: AirportViewEntity) {
        val intent = Intent().putExtra(EXTRA_AIRPORT, airport)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}