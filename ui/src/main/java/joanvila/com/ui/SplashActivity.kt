package joanvila.com.ui

import android.os.Bundle
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import joanvila.com.presentation.splash.SplashUiModel
import joanvila.com.presentation.splash.SplashViewModel
import joanvila.com.presentation.splashsplash.SplashIntent
import joanvila.com.ui.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity: BaseActivity<SplashIntent, SplashUiModel>() {

    private val viewModel by viewModel<SplashViewModel>()
    private var disposables = CompositeDisposable()

    override var layout = R.layout.activity_splash

    override fun onViewLoaded(savedInstanceState: Bundle?) {
        disposables.add(viewModel.states().subscribe(this::render))
        viewModel.processIntents(intents())
    }

    override fun intents(): Observable<SplashIntent> {
        return Observable.merge(listOf(initialIntent()))
    }

    override fun render(state: SplashUiModel) {
        when (state) {
            is SplashUiModel.Success -> {
                navigator.navigateToMainMenu()
                finish()
            }
            is SplashUiModel.Failed -> {
                showError(state.error)
            }
        }
    }

    private fun initialIntent(): Observable<SplashIntent.InitialIntent> {
        return Observable.just(SplashIntent.InitialIntent)
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

}