package joanvila.com.ui.base

import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.evernote.android.state.StateSaver
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import joanvila.com.presentation.base.BaseIntent
import joanvila.com.presentation.base.BaseView
import joanvila.com.presentation.base.BaseViewState
import joanvila.com.ui.navigation.Navigator
import joanvila.com.ui.utils.extractErrorMessage
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf



abstract class BaseActivity<I: BaseIntent, S: BaseViewState> : AppCompatActivity(), BaseView<I, S> {

    val navigator: Navigator by inject { parametersOf(this@BaseActivity) }

    abstract var layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        StateSaver.restoreInstanceState(this, savedInstanceState)
        onViewLoaded(savedInstanceState)
    }

    abstract fun onViewLoaded(savedInstanceState: Bundle?)

    protected fun showSnackBar(rootLayout: ViewGroup, message: String) {
        Snackbar.make(
                rootLayout, // Parent view
                message, // Message to show
                Snackbar.LENGTH_LONG //
        ).show()
    }

    override fun intents(): Observable<I> {
        return Observable.empty()
    }

    override fun render(state: S) {}

    protected fun showError(exception: Throwable?) {
        val errorMsg = exception?.let {
            it.extractErrorMessage(this)
        } ?: "Something wrong happened"
        showSnackBar(window.decorView.findViewById(android.R.id.content),
                errorMsg)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState.let { StateSaver.saveInstanceState(this, outState!!) }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return false
    }

}