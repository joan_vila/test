package joanvila.com.data

import io.reactivex.Single
import joanvila.com.data.base.Request
import joanvila.com.data.mapper.ScheduleMapper
import joanvila.com.data.remote.SchedulesService
import joanvila.com.data.remote.utils.ConnectionChecker
import joanvila.com.domain.model.Schedule
import joanvila.com.domain.repository.SchedulesRepository

class SchedulesDataRepository constructor(private val connectionChecker: ConnectionChecker,
                                          private val schedulesService: SchedulesService,
                                          private val scheduleMapper: ScheduleMapper) : SchedulesRepository {

    override fun getSchedules(params: HashMap<String, *>): Single<List<Schedule>> {
        val date = params[ApiConstants.SCHEDULE_DATE] as String? ?: ApiConstants.SCHEDULE_DEFAULT_DATE
        val origin = params[ApiConstants.SCHEDULE_ORIGIN] as String? ?: ApiConstants.SCHEDULE_DEFAULT_ORIGIN
        val arrival = params[ApiConstants.SCHEDULE_ARRIVAL] as String? ?: ApiConstants.SCHEDULE_DEFAULT_ARRIVAL
        val offset = params[ApiConstants.PAGINATION_OFFSET] as Int? ?: 0
        return Request.MakeSingle<List<Schedule>>(connectionChecker,
                schedulesService.getSchedules(origin, arrival, date, offset).map { response ->
                    response.resource.schedule.map {
                        scheduleMapper.mapFromEntity(it)
                    }
                }).buildRequest()
    }

}