package joanvila.com.data.cache

import android.content.Context
import android.content.SharedPreferences

open class PreferencesHelper(context: Context) {

    companion object {
        private val PREF_PACKAGE_NAME = "safeboda.preferences"
        private val KEY_TOKEN = "access_token"
    }

    private val preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(PREF_PACKAGE_NAME, Context.MODE_PRIVATE)
    }

    fun saveToken(token: String) {
        put(KEY_TOKEN, token)
    }

    fun getToken(): String? = get(KEY_TOKEN)

    private fun get(key: String) = preferences.getString(key, null)
    private fun put(key: String, value: String) = preferences.edit().putString(key, value).commit()

}