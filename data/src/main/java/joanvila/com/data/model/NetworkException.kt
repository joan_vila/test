package joanvila.com.data.model

sealed class NetworkException : Exception() {
    class NoInternetConnection : NetworkException()
    class ServerException : NetworkException()
    class UnprocessableEntityException(val error: String?) :
            NetworkException()
    class UnauthorizedException : NetworkException()
}
