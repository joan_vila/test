package joanvila.com.data.model

import com.google.gson.annotations.SerializedName

data class AirportsResponse(
        @SerializedName("AirportResource")
        val resource: AirportResource
)

data class AirportResource(
        @SerializedName("Airports")
        val airports: Airports
)

data class AirportEntity(
        @SerializedName("AirportCode")
        val airportCode: String,
        @SerializedName("CityCode")
        val cityCode: String,
        @SerializedName("Position")
        val position: Position
)

data class Position(
        @SerializedName("Coordinate")
        val coordinate: Coordinate
)

data class Coordinate(
        @SerializedName("Latitude")
        val latitude: Long,
        @SerializedName("Longitude")
        val longitude: Long
)

class Airports(vararg ms: AirportEntity) {
        @SerializedName("Airport")
        val airport = ms.toList()
}