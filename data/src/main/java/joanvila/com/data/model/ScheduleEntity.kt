package joanvila.com.data.model

import com.google.gson.annotations.SerializedName

data class SchedulesResponse(
        @SerializedName("ScheduleResource")
        val resource: ScheduleResource
)

data class ScheduleResource(
        @SerializedName("Schedule")
        val schedule: Schedules
)

data class ScheduleEntity(
        @SerializedName("Flight")
        val flights: Flights,
        @SerializedName("TotalJourney")
        val totalJourney: TotalJourney
)

data class TotalJourney(
        @SerializedName("Duration")
        val duration: String
)
data class FlightEntity(
        @SerializedName("Departure")
        val departure: ScheduledLocationEntity,
        @SerializedName("Arrival")
        val arrival: ScheduledLocationEntity,
        @SerializedName("MarketingCarrier")
        val marketingCarrier: MarketingCarrier
)

data class MarketingCarrier(
        @SerializedName("AirlineID")
        val airlineId: String,
        @SerializedName("FlightNumber")
        val flightNumber: Int
)

data class Stops(
        @SerializedName("StopsQuantity")
        val stopsQuantity: Int
)

data class ScheduledLocationEntity(
        @SerializedName("AirportCode")
        val airportCode: String,
        @SerializedName("ScheduledTimeLocal")
        val scheduledTimeLocal: ScheduledTimeLocal,
        @SerializedName("Terminal")
        val terminal: Terminal?
)

data class Terminal(
        @SerializedName("Name")
        val name: Int?
)
data class ScheduledTimeLocal(
        @SerializedName("DateTime")
        val dateTime: String
)

class Schedules(vararg ms: ScheduleEntity): ArrayList<ScheduleEntity>(ms.toList())
class Flights(vararg ms: FlightEntity): ArrayList<FlightEntity>(ms.toList())