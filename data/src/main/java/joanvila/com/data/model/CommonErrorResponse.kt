package joanvila.com.data.model

import com.google.gson.annotations.SerializedName

data class CommonErrorResponse(
        @SerializedName("ProcessingErrors")
        val processingErrors: ProcessingErrors
)
data class ProcessingErrors(
        @SerializedName("ProcessingError") val error : Error)

data class Error(@SerializedName("Code") var code: Int,
                 @SerializedName("Type") var type: String,
                 @SerializedName("Description") var description: String)