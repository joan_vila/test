package joanvila.com.data.model

import com.google.gson.annotations.SerializedName

data class AuthorizationResponse(
        @SerializedName("access_token")
        val accessToken: String
)