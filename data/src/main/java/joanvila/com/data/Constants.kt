package joanvila.com.data

class ApiConstants {
    companion object {
        const val BASE_URL = "https://api.lufthansa.com/v1/"
        const val API_KEY = "jktxxn4z4q2xx348g2esxq88"
        const val API_SECRET = "PPSN33P2sU"
        const val API_GRANT_TYPE = "client_credentials"
        const val AIRPORT_CODE = "code"
        const val SCHEDULE_DATE = "date"
        const val SCHEDULE_ORIGIN = "origin"
        const val SCHEDULE_ARRIVAL = "arrival"
        const val SCHEDULE_DEFAULT_DATE = "2018-12-30"
        const val SCHEDULE_DEFAULT_ORIGIN = "BCN"
        const val SCHEDULE_DEFAULT_ARRIVAL = "EBB"
        const val PAGINATION_OFFSET = "offset"
        const val PAGINATION_LIMIT = "limit"
        const val DEFAULT_LIMIT = 20
    }
}