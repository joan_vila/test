package joanvila.com.data.airports

import io.reactivex.Single
import joanvila.com.data.model.AirportEntity
import java.util.*

interface AirportsDataStore {
    fun getAirports(date: Date): Single<List<AirportEntity>>
}