package joanvila.com.data.base

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import joanvila.com.data.model.NetworkException
import joanvila.com.data.remote.utils.ConnectionChecker

sealed class Request<out RxSource> constructor(val connectionChecker: ConnectionChecker) {

    abstract fun buildRequest(): RxSource

    class MakeSingle<T> (connectionChecker: ConnectionChecker, private val request: Single<T>)
        : Request<Single<T>>(connectionChecker) {

        override fun buildRequest() : Single<T> {
            if (connectionChecker.thereIsConnectivity()) {
                return request.onErrorResumeNext {
                    Single.error(ErrorProcessor.process((it)))
                }
            }
            throw NetworkException.NoInternetConnection()
        }
    }

    class MakeCompletable(connectionChecker: ConnectionChecker, private val request: Completable)
        : Request<Completable>(connectionChecker) {

        override fun buildRequest(): Completable {
            if (connectionChecker.thereIsConnectivity()) {
                return request.onErrorResumeNext { Completable.error(ErrorProcessor.process(it)) }
            }
            throw NetworkException.NoInternetConnection()
        }

    }


    class MakeFlowable<T> (connectionChecker: ConnectionChecker, private val request: Flowable<T>)
        : Request<Flowable<T>>(connectionChecker) {

        override fun buildRequest(): Flowable<T> {
            if (connectionChecker.thereIsConnectivity()) {
                return request.onErrorResumeNext { error: Throwable ->
                    Flowable.error(ErrorProcessor.process(error))
                }
            }
            throw NetworkException.NoInternetConnection()
        }

    }

}