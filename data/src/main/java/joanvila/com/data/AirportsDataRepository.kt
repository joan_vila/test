package joanvila.com.data

import io.reactivex.Single
import joanvila.com.data.base.Request
import joanvila.com.data.mapper.AirportMapper
import joanvila.com.data.remote.AirportsService
import joanvila.com.data.remote.utils.ConnectionChecker
import joanvila.com.domain.model.Airport
import joanvila.com.domain.repository.AirportsRepository

class AirportsDataRepository constructor (private val connectionChecker: ConnectionChecker,
                              private val airportsService: AirportsService,
                              private val airportMapper: AirportMapper): AirportsRepository {

    override fun getAirports(params: HashMap<String, *>): Single<List<Airport>> {
        val code = params[ApiConstants.AIRPORT_CODE] as? String ?: ""
        val offset: Int = params[ApiConstants.PAGINATION_OFFSET] as? Int ?: 0
        val limit: Int = params[ApiConstants.PAGINATION_LIMIT] as? Int ?: ApiConstants.DEFAULT_LIMIT
        return Request.MakeSingle<List<Airport>>(connectionChecker,
                airportsService.getAirports(code, offset, limit).map { response ->
                    response.resource.airports.airport.map {
                        airportMapper.mapFromEntity(it)
                    }
                } ).buildRequest()
    }

    override fun getAirport(code: String): Single<Airport> {
        return Request.MakeSingle<Airport>(connectionChecker,
                airportsService.getAirport(code).map { response ->
                    val airport =  response.resource.airports.airport.first()
                    airportMapper.mapFromEntity(airport)
                } ).buildRequest()
    }

}