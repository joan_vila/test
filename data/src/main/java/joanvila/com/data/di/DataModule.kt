package joanvila.com.data.di

import domain.async.ThreadExecutor
import joanvila.com.data.AirportsDataRepository
import joanvila.com.data.AuthenticationDataRepository
import joanvila.com.data.SchedulesDataRepository
import joanvila.com.data.cache.PreferencesHelper
import joanvila.com.data.executor.JobExecutor
import joanvila.com.data.mapper.AirportMapper
import joanvila.com.data.mapper.FlightMapper
import joanvila.com.data.mapper.ScheduleMapper
import joanvila.com.data.mapper.ScheduledLocationMapper
import joanvila.com.data.remote.utils.ConnectionChecker
import joanvila.com.data.remote.utils.RetrofitFactory
import joanvila.com.domain.repository.AirportsRepository
import joanvila.com.domain.repository.AuthenticationRepository
import joanvila.com.domain.repository.SchedulesRepository
import org.koin.dsl.module.module

val dataModule = module {

    // Tasks
    single<AuthenticationRepository> { AuthenticationDataRepository(get(), get(), get()) }
    single<AirportsRepository> { AirportsDataRepository(get(), get(), get()) }
    single<SchedulesRepository> { SchedulesDataRepository(get(), get(), get()) }

    // Mappers
    single { AirportMapper() }
    single { ScheduleMapper(get()) }
    single { FlightMapper(get()) }
    single { ScheduledLocationMapper() }

    // Cache
    single { PreferencesHelper(get()) }

    // Remote
    single { RetrofitFactory.provideSchedulesService(RetrofitFactory.providesAuthorizedRetrofit(get()))}
    single { RetrofitFactory.provideAirportsService(RetrofitFactory.providesAuthorizedRetrofit(get()))}
    single { RetrofitFactory.provideAuthenticationService(RetrofitFactory.providesUnauthorizedRetrofit()) }
    single { ConnectionChecker(get()) }

    // Threads
    single<ThreadExecutor> { JobExecutor() }
}