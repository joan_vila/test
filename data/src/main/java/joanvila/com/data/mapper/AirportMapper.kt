package joanvila.com.data.mapper

import joanvila.com.data.model.AirportEntity
import joanvila.com.data.model.Coordinate
import joanvila.com.data.model.Position
import joanvila.com.domain.model.Airport

open class AirportMapper : Mapper<AirportEntity, Airport> {

    override fun mapFromEntity(type: AirportEntity): Airport {
        return Airport(
                type.airportCode,
                type.cityCode,
                type.position.coordinate.latitude,
                type.position.coordinate.longitude)
    }

    override fun mapToEntity(type: Airport): AirportEntity {
        return AirportEntity(
                type.id,
                type.name,
                Position(
                        Coordinate(
                                type.latitude,
                                type.longitude
                        )
                )
        )
    }

}