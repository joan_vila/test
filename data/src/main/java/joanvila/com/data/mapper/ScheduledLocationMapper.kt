package joanvila.com.data.mapper

import joanvila.com.data.model.ScheduledLocationEntity
import joanvila.com.data.model.ScheduledTimeLocal
import joanvila.com.data.model.Terminal
import joanvila.com.domain.model.ScheduledLocation

open class ScheduledLocationMapper : Mapper<ScheduledLocationEntity, ScheduledLocation> {

    override fun mapFromEntity(type: ScheduledLocationEntity): ScheduledLocation {
        return ScheduledLocation(
                type.airportCode,
                type.scheduledTimeLocal.dateTime,
                type.terminal?.name)
    }

    override fun mapToEntity(type: ScheduledLocation): ScheduledLocationEntity {
        return ScheduledLocationEntity(type.airportCode,
                ScheduledTimeLocal(type.dateTime),
                Terminal(type.terminalName))
    }

}