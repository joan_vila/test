package joanvila.com.data.mapper

import joanvila.com.data.model.*
import joanvila.com.domain.model.Schedule

open class ScheduleMapper (private val mapper: FlightMapper)
    : Mapper<ScheduleEntity, Schedule> {

    override fun mapFromEntity(type: ScheduleEntity): Schedule {
        return Schedule(
                type.totalJourney.duration,
                type.flights.map { mapper.mapFromEntity(it) }
        )
    }

    override fun mapToEntity(type: Schedule): ScheduleEntity {
        return ScheduleEntity(
                Flights(*(type.flights.map { mapper.mapToEntity(it) }).toTypedArray()),
                TotalJourney(type.duration)
        )
    }

}