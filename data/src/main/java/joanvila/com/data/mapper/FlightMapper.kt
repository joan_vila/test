package joanvila.com.data.mapper

import joanvila.com.data.model.*
import joanvila.com.domain.model.Flight
import joanvila.com.domain.model.ScheduledLocation

open class FlightMapper (private val mapper: ScheduledLocationMapper): Mapper<FlightEntity, Flight> {

    override fun mapFromEntity(type: FlightEntity): Flight {
        return Flight(
                type.marketingCarrier.flightNumber,
                mapper.mapFromEntity(type.departure),
                mapper.mapFromEntity(type.arrival)
        )

    }

    override fun mapToEntity(type: Flight): FlightEntity {
        return FlightEntity(
                mapper.mapToEntity(type.departure),
                mapper.mapToEntity(type.arrival),
                MarketingCarrier("", type.flightNumber)
        )
    }

}