package joanvila.com.data.remote

import io.reactivex.Single
import joanvila.com.data.model.AirportsResponse
import joanvila.com.data.model.AuthorizationResponse
import joanvila.com.data.model.Credentials
import joanvila.com.data.model.SchedulesResponse
import retrofit2.http.*

interface SchedulesService {

    @GET("operations/schedules/{origin}/{destination}/{fromDateTime}")
    fun getSchedules(@Path("origin") origin: String,
                    @Path("destination") destination: String,
                    @Path("fromDateTime") fromDateTime: String,
                    @Query("offset") offset: Int): Single<SchedulesResponse>

}