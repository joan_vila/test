package joanvila.com.data.remote.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import joanvila.com.data.model.AirportEntity
import joanvila.com.data.model.Airports
import joanvila.com.data.model.ScheduleEntity
import joanvila.com.data.model.Schedules
import java.lang.reflect.Type

class SchedulesDeserializer: JsonDeserializer<Schedules> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Schedules {
        return if (json.isJsonArray) {
            val schedules: Array<ScheduleEntity> = context?.deserialize<Array<ScheduleEntity>>(
                    json.asJsonArray,
                    Array<ScheduleEntity>::class.java) ?: emptyArray()
            Schedules(*schedules)
        } else if (json.isJsonObject) {
            Schedules(context?.deserialize<Any>(json.asJsonObject, ScheduleEntity::class.java) as ScheduleEntity)
        } else {
            throw JsonParseException("Unsupported type of Schedule element")
        }
    }
}