package joanvila.com.data.remote

import io.reactivex.Single
import joanvila.com.data.model.AirportsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AirportsService {


    @GET("references/airports/{code}")
    fun getAirports(@Path("code") code: String,
                    @Query("offset") offset: Int, @Query("limit") limit: Int): Single<AirportsResponse>

    @GET("references/airports/{code}?limit=1")
    fun getAirport(@Path("code") code: String): Single<AirportsResponse>
}