package joanvila.com.data.remote.utils

import com.google.gson.GsonBuilder
import joanvila.com.data.ApiConstants
import joanvila.com.data.cache.PreferencesHelper
import joanvila.com.data.model.Airports
import joanvila.com.data.model.Flights
import joanvila.com.data.model.Schedules
import joanvila.com.data.remote.AirportsService
import joanvila.com.data.remote.AuthorizationService
import joanvila.com.data.remote.SchedulesService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitFactory {

    fun providesAuthorizedRetrofit(preferences: PreferencesHelper): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addNetworkInterceptor {
                    chain ->
                    val request = chain.request().newBuilder()
                    request.addHeader("Authorization", "Bearer ${preferences.getToken()}")
                    chain.proceed(request.build())
                }
                .retryOnConnectionFailure(true)
                .addInterceptor(interceptor)
                .build()

        val gson = GsonBuilder()
                .registerTypeAdapter(Airports::class.java, AirportsDeserializer())
                .registerTypeAdapter(Schedules::class.java, SchedulesDeserializer())
                .registerTypeAdapter(Flights::class.java, FlightsDeserializer()).create()

        return Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(ApiConstants.BASE_URL)
                .build()
    }

    fun providesUnauthorizedRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client =  OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()

        return Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(ApiConstants.BASE_URL)
                .build()
    }

    fun provideSchedulesService(retrofit: Retrofit) = retrofit.create(SchedulesService::class.java)
    fun provideAirportsService(retrofit: Retrofit) = retrofit.create(AirportsService::class.java)
    fun provideAuthenticationService(retrofit: Retrofit) = retrofit.create(AuthorizationService::class.java)
}