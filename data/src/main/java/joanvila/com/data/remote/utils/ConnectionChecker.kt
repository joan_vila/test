package joanvila.com.data.remote.utils

import android.content.Context
import android.net.ConnectivityManager

class ConnectionChecker constructor(val context: Context) {

    fun thereIsConnectivity(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager?.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}