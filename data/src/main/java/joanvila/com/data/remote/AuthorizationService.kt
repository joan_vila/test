package joanvila.com.data.remote

import io.reactivex.Single
import joanvila.com.data.model.AuthorizationResponse
import joanvila.com.data.model.Credentials
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthorizationService {

    @FormUrlEncoded
    @POST("oauth/token")
    fun getToken(@Field("client_id") clientId: String,
                 @Field("client_secret") clientSecret: String,
                 @Field("grant_type") grantType: String): Single<AuthorizationResponse>

}