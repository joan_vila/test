package joanvila.com.data.remote.utils

import com.google.gson.Gson
import joanvila.com.data.model.CommonErrorResponse
import joanvila.com.data.model.NetworkException
import joanvila.com.data.model.ProcessingErrors
import okhttp3.ResponseBody
import retrofit2.Response

fun ResponseBody.extractErrorMessage() : String? {
    try {
        val errorResponse = Gson().fromJson(this.string(), CommonErrorResponse::class.java)
            return errorResponse.processingErrors.error.description
    } catch (e: Exception) {
        return e.localizedMessage
    }

}
