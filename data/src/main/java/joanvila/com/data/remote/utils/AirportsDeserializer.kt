package joanvila.com.data.remote.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import joanvila.com.data.model.AirportEntity
import joanvila.com.data.model.Airports
import java.lang.reflect.Type

class AirportsDeserializer: JsonDeserializer<Airports> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Airports {
        val airportElement = json.asJsonObject.get("Airport")
        return if (airportElement.isJsonArray) {
            val airports: Array<AirportEntity> = context?.deserialize<Array<AirportEntity>>(
                    airportElement.asJsonArray,
                    Array<AirportEntity>::class.java) ?: emptyArray()
            Airports(*airports)
        } else if (airportElement.isJsonObject) {
            Airports(context?.deserialize<Any>(airportElement.asJsonObject, AirportEntity::class.java) as AirportEntity)
        } else {
            throw JsonParseException("Unsupported type of airport element")
        }
    }
}