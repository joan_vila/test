package joanvila.com.data.remote.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import joanvila.com.data.model.*
import java.lang.reflect.Type

class FlightsDeserializer: JsonDeserializer<Flights> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Flights {
        return if (json.isJsonArray) {
            val flights: Array<FlightEntity> = context?.deserialize<Array<FlightEntity>>(
                    json.asJsonArray,
                    Array<FlightEntity>::class.java) ?: emptyArray()
            Flights(*flights)
        } else if (json.isJsonObject) {
            Flights(context?.deserialize<Any>(json.asJsonObject, FlightEntity::class.java) as FlightEntity)
        } else {
            throw JsonParseException("Unsupported type of Schedule element")
        }
    }
}