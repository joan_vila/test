package joanvila.com.data

import io.reactivex.Completable
import joanvila.com.data.base.Request
import joanvila.com.data.cache.PreferencesHelper
import joanvila.com.data.model.Credentials
import joanvila.com.data.remote.AuthorizationService
import joanvila.com.data.remote.utils.ConnectionChecker
import joanvila.com.domain.repository.AuthenticationRepository
import java.lang.Exception

class AuthenticationDataRepository(private val connectionChecker: ConnectionChecker,
                                   private val remoteDataSource: AuthorizationService,
                                   private val localDataSource: PreferencesHelper) : AuthenticationRepository {
    override fun authenticate(): Completable {
        return Request.MakeSingle(connectionChecker,
                remoteDataSource.getToken(ApiConstants.API_KEY, ApiConstants.API_SECRET, ApiConstants.API_GRANT_TYPE).map { it }).buildRequest()
                .flatMapCompletable { response ->
                    Completable.create { emitter ->
                        try {
                            localDataSource.saveToken(response.accessToken)
                            emitter.onComplete()
                        } catch (error: Exception) {
                            emitter.onError(Throwable("Could not save token"))
                        }
                    }
                }
    }


}