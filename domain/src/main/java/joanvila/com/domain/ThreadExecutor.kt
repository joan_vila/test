package domain.async

import java.util.concurrent.Executor

/**
 * Created by Borja on 3/1/17.
 */
interface ThreadExecutor: Executor {
}