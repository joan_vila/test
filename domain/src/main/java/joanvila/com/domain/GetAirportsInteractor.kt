package joanvila.com.domain

import domain.async.PostExecutionThread
import domain.async.ThreadExecutor
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import joanvila.com.domain.base.SingleUseCase
import joanvila.com.domain.model.Airport
import joanvila.com.domain.repository.AirportsRepository

class GetAirportsInteractor constructor(private val repository: AirportsRepository,
                                        private val threadExecutor: ThreadExecutor,
                                        private val postExecutionThread: PostExecutionThread):
        SingleUseCase<HashMap<String, *>, List<Airport>>() {

    override fun buildUseCaseObservable(params: HashMap<String, *>): Single<List<Airport>> {
        return repository.getAirports(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
    }
}