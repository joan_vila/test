package joanvila.com.domain.repository

import io.reactivex.Single
import joanvila.com.domain.model.Airport

interface AirportsRepository {
    fun getAirports(params: HashMap<String, *>): Single<List<Airport>>
    fun getAirport(code: String): Single<Airport>
}