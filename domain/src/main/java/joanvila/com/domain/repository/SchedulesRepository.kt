package joanvila.com.domain.repository

import io.reactivex.Single
import joanvila.com.domain.model.Schedule

interface SchedulesRepository {
    fun getSchedules(params: HashMap<String, *>): Single<List<Schedule>>
}