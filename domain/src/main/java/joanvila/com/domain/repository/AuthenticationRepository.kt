package joanvila.com.domain.repository

import io.reactivex.Completable

interface AuthenticationRepository{
    fun authenticate(): Completable
}