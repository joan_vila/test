package joanvila.com.domain

import domain.async.PostExecutionThread
import domain.async.ThreadExecutor
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import joanvila.com.domain.base.CompletableUseCase
import joanvila.com.domain.repository.AuthenticationRepository

class AuthenticateInteractor constructor(private val repository: AuthenticationRepository,
                                         private val threadExecutor: ThreadExecutor,
                                         private val postExecutionThread: PostExecutionThread):
        CompletableUseCase<Unit>() {
    override fun buildUseCaseObservable(params: Unit): Completable {
        return repository.authenticate()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
    }
}