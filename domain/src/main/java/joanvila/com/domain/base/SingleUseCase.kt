package joanvila.com.domain.base

import io.reactivex.Single

abstract class SingleUseCase<in Params, T>  {

    /**
     * Builds a [Completable] which will be used when the current [SingleUseCase] is executed.
     */
    protected abstract fun buildUseCaseObservable(params: Params): Single<T>

    /**
     * Executes the current use case.
     */
    fun execute(params: Params) = buildUseCaseObservable(params)

}