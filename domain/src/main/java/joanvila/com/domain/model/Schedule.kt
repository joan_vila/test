package joanvila.com.domain.model

data class Schedule(
        val duration: String,
        val flights: List<Flight>
)

data class Flight(
        val flightNumber: Int,
        val departure: ScheduledLocation,
        val arrival: ScheduledLocation
)

data class ScheduledLocation(
        val airportCode: String,
        val dateTime: String,
        val terminalName: Int?
)