package joanvila.com.domain.model

data class Airport(
        val id: String,
        val name: String,
        val latitude: Long,
        val longitude: Long
)