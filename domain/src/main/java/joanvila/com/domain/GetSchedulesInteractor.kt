package joanvila.com.domain

import domain.async.PostExecutionThread
import domain.async.ThreadExecutor
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import joanvila.com.domain.base.SingleUseCase
import joanvila.com.domain.model.Schedule
import joanvila.com.domain.repository.SchedulesRepository

class GetSchedulesInteractor constructor(private val repository: SchedulesRepository,
                                         private val threadExecutor: ThreadExecutor,
                                         private val postExecutionThread: PostExecutionThread):
        SingleUseCase<HashMap<String, *>, List<Schedule>>() {

    override fun buildUseCaseObservable(params: HashMap<String, *>): Single<List<Schedule>> {
        return repository.getSchedules(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
    }
}