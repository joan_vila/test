package domain.async

import io.reactivex.Scheduler

/**
 * Created by Borja on 3/1/17.
 */
interface PostExecutionThread {

    val scheduler: Scheduler
}