package joanvila.com.domain

import domain.async.PostExecutionThread
import domain.async.ThreadExecutor
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import joanvila.com.domain.base.SingleUseCase
import joanvila.com.domain.model.Airport
import joanvila.com.domain.repository.AirportsRepository

class GetAirportInteractor constructor(private val repository: AirportsRepository,
                                       private val threadExecutor: ThreadExecutor,
                                       private val postExecutionThread: PostExecutionThread):
        SingleUseCase<String, Airport>() {

    override fun buildUseCaseObservable(params: String): Single<Airport> {
        return repository.getAirport(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
    }
}