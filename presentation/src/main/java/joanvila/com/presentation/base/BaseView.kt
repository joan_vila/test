package joanvila.com.presentation.base

import io.reactivex.Observable

/**
 * Created by Borja on 23/12/16.
 */
interface BaseView<I: BaseIntent, S: BaseViewState> {
    fun intents(): Observable<I>
    fun render(state: S)
}