package joanvila.com.presentation.utils

import android.os.Parcelable

interface ViewType: Parcelable {
    fun getViewType(): Int
}