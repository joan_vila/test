package joanvila.com.presentation.utils

object ViewTypeConstants {
    const val VIEW_TYPE_AIRPORT = 1
    const val VIEW_TYPE_SCHEDULE = 2
    const val VIEW_TYPE_LOADING = 99
}
