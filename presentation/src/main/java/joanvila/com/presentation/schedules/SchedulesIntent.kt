package joanvila.com.presentation.schedules

import joanvila.com.presentation.base.BaseIntent

sealed class SchedulesIntent: BaseIntent {

    data class InitialIntent(
            val date: String,
            val origin: String,
            val arrival: String): SchedulesIntent()

    data class OnLoadMore(val date: String,
                          val origin: String,
                          val arrival: String,
                          val offset: Int): SchedulesIntent()

    data class SelectDate(val date: String,
                          val origin: String,
                          val arrival: String): SchedulesIntent()
}