package joanvila.com.presentation.schedules

import joanvila.com.presentation.base.BaseViewState

sealed class SchedulesUiModel(
        val inProgress: Boolean = false,
        val error: Throwable? = null,
        val isLoadingMore: Boolean = false
): BaseViewState {
    data class InProgress(val refresh: Boolean) : SchedulesUiModel(true, null, !refresh)
    data class Failed(val exception: Throwable?) : SchedulesUiModel(false, exception)
    data class Success(val data: List<ScheduleViewEntity>?, val loadMore: Boolean) : SchedulesUiModel(false, null, loadMore)
    object Idle : SchedulesUiModel(false)
}