package joanvila.com.presentation.schedules

import android.os.Parcelable
import joanvila.com.domain.model.Flight
import joanvila.com.presentation.utils.ViewType
import joanvila.com.presentation.utils.ViewTypeConstants
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleViewEntity(
        val duration: String,
        val flights: List<FlightViewEntity>
) : ViewType {
    override fun getViewType() = ViewTypeConstants.VIEW_TYPE_SCHEDULE
}

@Parcelize
data class FlightViewEntity(
        val flightNumber: Int,
        val departure: ScheduledLocationViewEntity,
        val arrival: ScheduledLocationViewEntity
): Parcelable

@Parcelize
data class ScheduledLocationViewEntity(
        val airportCode: String,
        val dateTime: String,
        val terminalName: Int?): Parcelable
