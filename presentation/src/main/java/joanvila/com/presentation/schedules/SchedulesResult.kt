package joanvila.com.presentation.schedules

import joanvila.com.presentation.base.BaseResult
import joanvila.com.presentation.model.TaskStatus

sealed class SchedulesResult: BaseResult {
    data class GetSchedules(val status: TaskStatus,
                            val error: Throwable? = null,
                            val loadMore: Boolean = false,
                            val refresh: Boolean = false,
                            val data: List<ScheduleViewEntity>? = null): SchedulesResult()
}