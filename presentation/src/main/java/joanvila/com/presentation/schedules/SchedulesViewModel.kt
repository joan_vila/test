package joanvila.com.presentation.schedules

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.base.BaseViewModel
import joanvila.com.presentation.model.TaskStatus

class SchedulesViewModel constructor(private val schedulesProcessor: SchedulesProcessor
        ): ViewModel(), BaseViewModel<SchedulesIntent, SchedulesUiModel> {

    private val intentsSubject: PublishSubject<SchedulesIntent> = PublishSubject.create()
    private val statesObservable: Observable<SchedulesUiModel> = compose()

    override fun processIntents(intents: Observable<SchedulesIntent>) {
        intents.subscribe(intentsSubject)
    }

    override fun states() = statesObservable.onErrorResumeNext { error: Throwable ->
        Observable.just(SchedulesUiModel.Failed(error))
    }

    private val intentFilter: ObservableTransformer<SchedulesIntent, SchedulesIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge<SchedulesIntent>(
                        shared.ofType(SchedulesIntent.InitialIntent::class.java).take(1),
                        shared.filter { it !is SchedulesIntent.InitialIntent }
                )
            }
        }

    private fun compose() =
            intentsSubject
                    .compose<SchedulesIntent>(intentFilter)
                    .map { this.actionFromIntent(it) }
                    .compose(schedulesProcessor.actionProcessor)
                    .scan(SchedulesUiModel.Idle, reducer)
                    .distinctUntilChanged()
                    .replay(1)
                    .autoConnect(0)

    private fun actionFromIntent(intent: SchedulesIntent): SchedulesAction {
        return when (intent) {
            is SchedulesIntent.InitialIntent -> SchedulesAction.LoadSchedules(
                    intent.date,
                    intent.origin,
                    intent.arrival,
                    0,
                    true)
            is SchedulesIntent.SelectDate -> SchedulesAction.LoadSchedules(
                    intent.date,
                    intent.origin,
                    intent.arrival,
                    0,
                    true)
            is SchedulesIntent.OnLoadMore -> SchedulesAction.LoadSchedules(
                    intent.date,
                    intent.origin,
                    intent.arrival,
                    intent.offset,
                    false)
        }
    }

    companion object {
        private val reducer =
                BiFunction { _: SchedulesUiModel, result: SchedulesResult ->
                    when (result) {
                        is SchedulesResult.GetSchedules -> {
                            when {
                                result.status == TaskStatus.SUCCESS -> SchedulesUiModel.Success(result.data, result.loadMore)
                                result.status == TaskStatus.FAILURE -> SchedulesUiModel.Failed(result.error)
                                result.status == TaskStatus.IN_FLIGHT -> SchedulesUiModel.InProgress(result.refresh)
                                else -> SchedulesUiModel.Idle
                            }
                        }
                    }
                }
    }

}