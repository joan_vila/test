package joanvila.com.presentation.schedules

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import joanvila.com.data.ApiConstants
import joanvila.com.domain.GetSchedulesInteractor
import joanvila.com.presentation.mapper.ScheduleMapper
import joanvila.com.presentation.model.TaskStatus

class SchedulesProcessor(private val getAirportsInteractor: GetSchedulesInteractor,
                         private val scheduleMapper: ScheduleMapper) {

    private val loadSchedulesProcessor =
            ObservableTransformer<SchedulesAction.LoadSchedules, SchedulesResult> { intents ->
                intents.flatMap { intent ->
                    val params = hashMapOf(
                            ApiConstants.SCHEDULE_DATE to intent.date,
                            ApiConstants.PAGINATION_OFFSET to intent.offset,
                            ApiConstants.SCHEDULE_ORIGIN to intent.origin,
                            ApiConstants.SCHEDULE_ARRIVAL to intent.arrival
                    )
                    getAirportsInteractor.execute(params)
                            .map { schedules ->
                                SchedulesResult.GetSchedules(TaskStatus.SUCCESS, null,
                                        intent.offset > 1,
                                        intent.refresh,
                                        schedules.map { scheduleMapper.mapFromDomainEntity(it) })
                            }
                            .onErrorReturn { error ->
                                SchedulesResult.GetSchedules(TaskStatus.FAILURE, error)
                            }
                            .toObservable()
                            .startWith(SchedulesResult.GetSchedules(TaskStatus.IN_FLIGHT, null, intent.offset > 1, intent.refresh))
                }
            }

    internal var actionProcessor =
            ObservableTransformer<SchedulesAction, SchedulesResult> { intents ->
                intents.publish { shared ->
                    Observable.merge(arrayListOf(
                            shared.ofType(SchedulesAction.LoadSchedules::class.java).compose(loadSchedulesProcessor)
                    ))
                }
            }

}