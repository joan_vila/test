package joanvila.com.presentation.schedules

import joanvila.com.presentation.base.BaseAction

sealed class SchedulesAction: BaseAction {
    data class LoadSchedules(val date: String, val origin: String, val arrival: String,
                             val offset: Int, val refresh: Boolean): SchedulesAction()
}

