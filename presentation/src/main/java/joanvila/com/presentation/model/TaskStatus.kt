package joanvila.com.presentation.model

enum class TaskStatus {
    SUCCESS, FAILURE, IN_FLIGHT
}