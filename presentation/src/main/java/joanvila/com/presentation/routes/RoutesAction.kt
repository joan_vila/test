package joanvila.com.presentation.routes

import joanvila.com.presentation.base.BaseAction
import joanvila.com.presentation.schedules.ScheduleViewEntity

sealed class RoutesAction: BaseAction {
    object Idle: RoutesAction()
    data class LoadRoute(val schedule: ScheduleViewEntity): RoutesAction()
}

