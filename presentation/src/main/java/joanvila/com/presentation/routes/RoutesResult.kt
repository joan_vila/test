package joanvila.com.presentation.routes

import joanvila.com.presentation.base.BaseResult
import joanvila.com.presentation.model.TaskStatus

sealed class RoutesResult: BaseResult {
    data class GetRoute(val status: TaskStatus,
                        val error: Throwable? = null,
                        val data: List<Pair<Long,Long>>? = null): RoutesResult()
}