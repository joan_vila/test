package joanvila.com.presentation.routes

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.base.BaseViewModel
import joanvila.com.presentation.model.TaskStatus

class RoutesViewModel constructor(private val rooutesProcessor: RoutesProcessor
        ): ViewModel(), BaseViewModel<RoutesIntent, RoutesUiModel> {

    private val intentsSubject: PublishSubject<RoutesIntent> = PublishSubject.create()
    private val statesObservable: Observable<RoutesUiModel> = compose()

    override fun processIntents(intents: Observable<RoutesIntent>) {
        intents.subscribe(intentsSubject)
    }

    override fun states() = statesObservable.onErrorResumeNext { error: Throwable ->
        Observable.just(RoutesUiModel.Failed(error))
    }

    private val intentFilter: ObservableTransformer<RoutesIntent, RoutesIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge<RoutesIntent>(
                        shared.ofType(RoutesIntent.InitialIntent::class.java).take(1),
                        shared.filter { it !is RoutesIntent.InitialIntent }
                )
            }
        }

    private fun compose() =
            intentsSubject
                    .compose<RoutesIntent>(intentFilter)
                    .map { this.actionFromIntent(it) }
                    .compose(rooutesProcessor.actionProcessor)
                    .scan(RoutesUiModel.Idle, reducer)
                    .distinctUntilChanged()
                    .replay(1)
                    .autoConnect(0)

    private fun actionFromIntent(intent: RoutesIntent): RoutesAction {
        return when (intent) {
            is RoutesIntent.InitialIntent -> RoutesAction.Idle
            is RoutesIntent.OnMapaLoaded -> RoutesAction.LoadRoute(intent.schedule)
        }
    }

    companion object {
        private val reducer =
                BiFunction { _: RoutesUiModel, result: RoutesResult ->
                    when (result) {
                        is RoutesResult.GetRoute -> {
                            when {
                                result.status == TaskStatus.SUCCESS -> RoutesUiModel.Success(result.data)
                                result.status == TaskStatus.FAILURE -> RoutesUiModel.Failed(result.error)
                                result.status == TaskStatus.IN_FLIGHT -> RoutesUiModel.InProgress
                                else -> RoutesUiModel.Idle
                            }
                        }
                    }
                }
    }

}