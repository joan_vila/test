package joanvila.com.presentation.routes

import joanvila.com.presentation.base.BaseViewState

sealed class RoutesUiModel(
        val inProgress: Boolean = false,
        val error: Throwable? = null
): BaseViewState {
    object InProgress : RoutesUiModel(true)
    data class Failed(val exception: Throwable?) : RoutesUiModel(false, exception)
    data class Success(val data: List<Pair<Long, Long>>?) : RoutesUiModel(false, null)
    object Idle : RoutesUiModel(false)
}