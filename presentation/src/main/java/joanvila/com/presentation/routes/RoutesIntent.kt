package joanvila.com.presentation.routes

import joanvila.com.presentation.base.BaseIntent
import joanvila.com.presentation.schedules.ScheduleViewEntity

sealed class RoutesIntent: BaseIntent {
    object InitialIntent: RoutesIntent()
    data class OnMapaLoaded(val schedule: ScheduleViewEntity): RoutesIntent()
}