package joanvila.com.presentation.routes

import androidx.lifecycle.Transformations.map
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import joanvila.com.domain.GetAirportInteractor
import joanvila.com.domain.model.Airport
import joanvila.com.presentation.model.TaskStatus
import joanvila.com.presentation.schedules.FlightViewEntity
import java.util.*

class RoutesProcessor(private val getAirportInteractor: GetAirportInteractor) {

    private val loadRoutesProcessor =
            ObservableTransformer<RoutesAction.LoadRoute, RoutesResult> { intents ->
                intents.switchMap { intent ->
                            val observables = arrayListOf<Single<Pair<Airport, Airport>>>()
                            intent.schedule.flights.map {
                                observables.add(getFlightAirports(it))
                            }
                            Single.zip(observables) {
                                val airportPairs =  it.filterIsInstance<Pair<Airport,Airport>>()
                                val airports = arrayListOf<Airport>()
                                airportPairs.forEach { airportPair ->
                                    airports.addAll(arrayListOf(airportPair.first, airportPair.second))
                                }
                                airports
                            }
                            .map { airports ->
                                val schedule = intent.schedule
                                val coordinates: ArrayList<Pair<Long, Long>> = arrayListOf()
                                schedule.flights.forEach { flight ->
                                    airports.first { it.id == flight.departure.airportCode }.let {
                                        coordinates.add(Pair(it.latitude, it.longitude))
                                    }
                                    airports.first { it.id == flight.arrival.airportCode }.let {
                                        coordinates.add(Pair(it.latitude, it.longitude))
                                    }
                                }
                                RoutesResult.GetRoute(TaskStatus.SUCCESS, null, coordinates)
                            }.toObservable()
                            .onErrorReturn { error ->
                                RoutesResult.GetRoute(TaskStatus.FAILURE, error)
                            }.startWith(RoutesResult.GetRoute(TaskStatus.IN_FLIGHT))
                }
            }

    private fun getFlightAirports(flight: FlightViewEntity): Single<Pair<Airport, Airport>> {
        val arrivalCode = flight.arrival.airportCode
        val departureCode = flight.departure.airportCode
        return Single.zip(getAirportInteractor.execute(arrivalCode),
                getAirportInteractor.execute(departureCode),
                BiFunction { arrival: Airport, destination: Airport ->
                    Pair(arrival, destination)
                })
    }

    internal var actionProcessor =
            ObservableTransformer<RoutesAction, RoutesResult> { intents ->
                intents.publish { shared ->
                    Observable.merge(arrayListOf(
                            shared.ofType(RoutesAction.LoadRoute::class.java).compose(loadRoutesProcessor)
                    ))
                }
            }

}