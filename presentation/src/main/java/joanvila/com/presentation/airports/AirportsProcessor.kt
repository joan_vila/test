package joanvila.com.presentation.airports

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import joanvila.com.data.ApiConstants
import joanvila.com.domain.GetAirportsInteractor
import joanvila.com.presentation.mapper.AirportMapper
import joanvila.com.presentation.model.TaskStatus

class AirportsProcessor(private val getAirportsInteractor: GetAirportsInteractor,
                        private val airportMapper: AirportMapper) {

    private val loadAirportsProcessor =
            ObservableTransformer<AirportsAction.LoadAirports, AirportsResult> { intents ->
                intents.flatMap { intent ->
                    val params = hashMapOf(
                            ApiConstants.AIRPORT_CODE to intent.code,
                            ApiConstants.PAGINATION_OFFSET to intent.offset
                    )
                    getAirportsInteractor.execute(params)
                            .map { airports ->
                                AirportsResult.GetAirports(TaskStatus.SUCCESS, null,
                                        intent.offset > 0,
                                        airports.map { airportMapper.mapFromDomainEntity(it) })
                            }
                            .onErrorReturn { error ->
                                AirportsResult.GetAirports(TaskStatus.FAILURE, error)
                            }
                            .toObservable()
                            .startWith(AirportsResult.GetAirports(TaskStatus.IN_FLIGHT))
                }
            }

    internal var actionProcessor =
            ObservableTransformer<AirportsAction, AirportsResult> { intents ->
                intents.publish { shared ->
                    Observable.merge(arrayListOf(
                            shared.ofType(AirportsAction.LoadAirports::class.java).compose(loadAirportsProcessor)
                    ))
                }
            }

}