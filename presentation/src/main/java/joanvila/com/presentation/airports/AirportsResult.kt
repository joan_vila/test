package joanvila.com.presentation.airports

import joanvila.com.presentation.base.BaseResult
import joanvila.com.presentation.model.TaskStatus

sealed class AirportsResult: BaseResult {
    data class GetAirports(val status: TaskStatus,
                           val error: Throwable? = null,
                           val loadMore: Boolean = false,
                           val data: List<AirportViewEntity>? = null): AirportsResult()
}