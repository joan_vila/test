package joanvila.com.presentation.airports

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.base.BaseViewModel
import joanvila.com.presentation.model.TaskStatus

class AirportsViewModel constructor(private val airportsProcessor: AirportsProcessor
        ): ViewModel(), BaseViewModel<AirportsIntent, AirportsUiModel> {

    private val intentsSubject: PublishSubject<AirportsIntent> = PublishSubject.create()
    private val statesObservable: Observable<AirportsUiModel> = compose()

    override fun processIntents(intents: Observable<AirportsIntent>) {
        intents.subscribe(intentsSubject)
    }

    override fun states() = statesObservable.onErrorResumeNext { error: Throwable ->
        Observable.just(AirportsUiModel.Failed(error))
    }

    private val intentFilter: ObservableTransformer<AirportsIntent, AirportsIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge<AirportsIntent>(
                        shared.ofType(AirportsIntent.InitialIntent::class.java).take(1),
                        shared.filter { it !is AirportsIntent.InitialIntent }
                )
            }
        }

    private fun compose() =
            intentsSubject
                    .compose<AirportsIntent>(intentFilter)
                    .map { this.actionFromIntent(it) }
                    .compose(airportsProcessor.actionProcessor)
                    .scan(AirportsUiModel.Idle, reducer)
                    .distinctUntilChanged()
                    .replay(1)
                    .autoConnect(0)

    private fun actionFromIntent(intent: AirportsIntent): AirportsAction {
        return when (intent) {
            is AirportsIntent.InitialIntent ->
                AirportsAction.Initial()
            is AirportsIntent.EnterName ->
                AirportsAction.LoadAirports(intent.name ?: "", 0)
            is AirportsIntent.OnLoadMore ->
                AirportsAction.LoadAirports(intent.name ?: "", intent.offset)
        }
    }

    companion object {
        private val reducer =
                BiFunction { _: AirportsUiModel, result:
                AirportsResult ->
                    when (result) {
                        is AirportsResult.GetAirports-> {
                            when {
                                result.status == TaskStatus.SUCCESS -> AirportsUiModel.Success(result.data, result.loadMore)
                                result.status == TaskStatus.FAILURE -> AirportsUiModel.Failed(result.error)
                                result.status == TaskStatus.IN_FLIGHT -> AirportsUiModel.InProgress
                                else -> AirportsUiModel.Idle
                            }
                        }
                    }
                }
    }

}