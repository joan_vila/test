package joanvila.com.presentation.airports

import joanvila.com.presentation.base.BaseIntent

sealed class AirportsIntent : BaseIntent {
    object InitialIntent: AirportsIntent()
    data class EnterName(val name: String? = null): AirportsIntent()
    data class OnLoadMore(val name: String?, val offset: Int): AirportsIntent()
}