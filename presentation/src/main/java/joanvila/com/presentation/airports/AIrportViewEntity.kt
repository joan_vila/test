package joanvila.com.presentation.airports

import joanvila.com.presentation.utils.ViewType
import joanvila.com.presentation.utils.ViewTypeConstants
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AirportViewEntity(
        val id: String,
        val name: String,
        val latitude: Long,
        val longitude: Long
) : ViewType {
    override fun getViewType() = ViewTypeConstants.VIEW_TYPE_AIRPORT
}