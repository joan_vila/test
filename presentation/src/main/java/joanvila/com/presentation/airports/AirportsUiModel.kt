package joanvila.com.presentation.airports

import joanvila.com.presentation.base.BaseViewState

sealed class AirportsUiModel(
        val inProgress: Boolean = false,
        val error: Throwable? = null
): BaseViewState {
    object InProgress : AirportsUiModel(true)
    data class Failed(val exception: Throwable?) : AirportsUiModel(false, exception)
    data class Success(val data: List<AirportViewEntity>?, val loadMore: Boolean) : AirportsUiModel(false)
    object Idle : AirportsUiModel(false)
}