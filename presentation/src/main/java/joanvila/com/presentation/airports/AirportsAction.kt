package joanvila.com.presentation.airports

import joanvila.com.presentation.base.BaseAction

sealed class AirportsAction: BaseAction {
    open class Initial: AirportsAction()
    data class LoadAirports(val code: String, val offset: Int): AirportsAction()
}

