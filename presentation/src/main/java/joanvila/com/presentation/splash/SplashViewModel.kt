package joanvila.com.presentation.splash

import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import joanvila.com.presentation.base.BaseViewModel
import joanvila.com.presentation.model.TaskStatus
import joanvila.com.presentation.splashsplash.SplashIntent


class SplashViewModel (private val splashProcessor: SplashProcessor): ViewModel(),
        BaseViewModel<SplashIntent, SplashUiModel> {

    private val intentsSubject: PublishSubject<SplashIntent> = PublishSubject.create()
    private val statesObservable: Observable<SplashUiModel> = compose()

    private val intentFilter: ObservableTransformer<SplashIntent, SplashIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge<SplashIntent>(
                        shared.ofType(SplashIntent.InitialIntent::class.java).take(1),
                        shared.filter { it !is SplashIntent.InitialIntent }
                )
            }
        }

    override fun processIntents(intents: Observable<SplashIntent>) {
        intents.subscribe(intentsSubject)
    }

    override fun states() = statesObservable.onErrorResumeNext { error: Throwable ->
        Observable.just(SplashUiModel.Failed(error))
    }

    private fun compose() =
            intentsSubject
                    .compose<SplashIntent>(intentFilter)
                    .map { this.actionFromIntent(it) }
                    .compose(splashProcessor.actionProcessor)
                    .scan(SplashUiModel.Idle(), reducer)
                    .distinctUntilChanged()
                    .replay(1)
                    .autoConnect(0)

    private fun actionFromIntent(intent: SplashIntent): SplashAction {
        return when (intent) {
            is SplashIntent.InitialIntent -> SplashAction.Authorize()
        }
    }

    companion object {

        private val reducer =
                BiFunction {
                    _: SplashUiModel, result: SplashResult ->
                    when (result) {
                        is SplashResult.MakeSplashTask -> {
                            when {
                                result.status == TaskStatus.SUCCESS -> SplashUiModel.Success
                                result.status == TaskStatus.FAILURE -> SplashUiModel.Failed(result.error)
                                else -> SplashUiModel.Idle()
                            }
                        }
                    }
                }

    }
}