package joanvila.com.presentation.splash

import joanvila.com.presentation.base.BaseResult
import joanvila.com.presentation.model.TaskStatus

sealed class SplashResult: BaseResult {
    class MakeSplashTask(val status: TaskStatus, val error: Throwable? =
            null): SplashResult() {
        internal fun success(): MakeSplashTask {
            return MakeSplashTask(TaskStatus.SUCCESS)
        }
        internal fun failure(error: Throwable): MakeSplashTask {
            return MakeSplashTask(TaskStatus.FAILURE, error)
        }
    }
}