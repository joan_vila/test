package joanvila.com.presentation.splashsplash

import joanvila.com.presentation.base.BaseIntent

sealed class SplashIntent: BaseIntent {
    object InitialIntent: SplashIntent()
}