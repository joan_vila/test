package joanvila.com.presentation.splash

import joanvila.com.presentation.base.BaseViewState

sealed class SplashUiModel(val success: Boolean? = false,
                           val error: Throwable? = null
): BaseViewState  {
    object Success : SplashUiModel(true,  null)
    data class Failed(val exception: Throwable?) : SplashUiModel(null, exception)
    class Idle : SplashUiModel()
}