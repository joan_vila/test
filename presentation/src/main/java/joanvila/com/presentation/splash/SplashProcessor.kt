package joanvila.com.presentation.splash

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import joanvila.com.domain.AuthenticateInteractor
import joanvila.com.presentation.model.TaskStatus

class SplashProcessor(private val authorizationInteractor: AuthenticateInteractor) {

    private val splashProcessor =
            ObservableTransformer<SplashAction.Authorize, SplashResult> { intents ->
                intents.switchMap {
                    authorizationInteractor.execute(Unit)
                            .andThen( Single.just(SplashResult.MakeSplashTask(TaskStatus.SUCCESS) ))
                            .toObservable()
                            .onErrorReturn { error ->
                                SplashResult.MakeSplashTask(TaskStatus.FAILURE, error)
                            }
                }
            }

    internal var actionProcessor =
            ObservableTransformer<SplashAction, SplashResult> { intents ->
                intents.publish { shared ->
                    Observable.merge<SplashResult>(listOf(
                            shared.ofType(SplashAction.Authorize::class.java).compose(splashProcessor)
                    ))
                }
            }
}