package joanvila.com.presentation.splash

import joanvila.com.presentation.base.BaseAction

sealed class SplashAction: BaseAction {
    open class Authorize: SplashAction()
}