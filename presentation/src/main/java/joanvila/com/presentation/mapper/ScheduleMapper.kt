package joanvila.com.presentation.mapper

import joanvila.com.domain.model.Schedule
import joanvila.com.presentation.schedules.ScheduleViewEntity

open class ScheduleMapper (private val mapper: FlightMapper)
    : Mapper<Schedule, ScheduleViewEntity> {

    override fun mapFromDomainEntity(type: Schedule): ScheduleViewEntity {
        return ScheduleViewEntity(
                type.duration,
                type.flights.map { mapper.mapFromDomainEntity(it) }
        )
    }

    override fun mapToDomainEntity(type: ScheduleViewEntity): Schedule {
        return Schedule(
                type.duration,
                type.flights.map { mapper.mapToDomainEntity(it) }
        )
    }

}