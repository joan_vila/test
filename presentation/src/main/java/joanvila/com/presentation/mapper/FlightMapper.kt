package joanvila.com.presentation.mapper

import joanvila.com.domain.model.Flight
import joanvila.com.presentation.schedules.FlightViewEntity

open class FlightMapper (private val mapper: ScheduledLocationMapper)
    : Mapper<Flight, FlightViewEntity> {

    override fun mapFromDomainEntity(type: Flight): FlightViewEntity {
        return FlightViewEntity(
                type.flightNumber,
                mapper.mapFromDomainEntity(type.departure),
                mapper.mapFromDomainEntity(type.arrival)
        )
    }

    override fun mapToDomainEntity(type: FlightViewEntity): Flight {
        return Flight(
                type.flightNumber,
                mapper.mapToDomainEntity(type.departure),
                mapper.mapToDomainEntity(type.arrival)
        )
    }


}