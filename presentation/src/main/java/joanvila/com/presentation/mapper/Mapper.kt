package joanvila.com.presentation.mapper

interface Mapper<D, V> {

    fun mapFromDomainEntity(type: D): V

    fun mapToDomainEntity(type: V): D

}