package joanvila.com.presentation.mapper

import joanvila.com.domain.model.Airport
import joanvila.com.presentation.airports.AirportViewEntity

open class AirportMapper :
        Mapper<Airport, AirportViewEntity> {

    override fun mapFromDomainEntity(type: Airport): AirportViewEntity {
        return AirportViewEntity(
                type.id,
                type.name,
                type.latitude,
                type.longitude
        )
    }

    override fun mapToDomainEntity(type: AirportViewEntity): Airport {
        return Airport(
                type.id,
                type.name,
                type.latitude,
                type.longitude
        )
    }

}