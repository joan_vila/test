package joanvila.com.presentation.mapper

import joanvila.com.domain.model.ScheduledLocation
import joanvila.com.presentation.schedules.ScheduledLocationViewEntity

open class ScheduledLocationMapper :
        Mapper<ScheduledLocation, ScheduledLocationViewEntity> {

    override fun mapFromDomainEntity(type: ScheduledLocation): ScheduledLocationViewEntity {
        return ScheduledLocationViewEntity(
                type.airportCode,
                type.dateTime,
                type.terminalName
        )
    }

    override fun mapToDomainEntity(type: ScheduledLocationViewEntity): ScheduledLocation {
        return ScheduledLocation(
                type.airportCode,
                type.dateTime,
                type.terminalName
        )
    }


}