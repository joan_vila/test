package joanvila.com.presentation.di

import joanvila.com.domain.AuthenticateInteractor
import joanvila.com.domain.GetAirportInteractor
import joanvila.com.domain.GetAirportsInteractor
import joanvila.com.domain.GetSchedulesInteractor
import joanvila.com.presentation.airports.AirportsProcessor
import joanvila.com.presentation.airports.AirportsViewModel
import joanvila.com.presentation.mapper.AirportMapper
import joanvila.com.presentation.mapper.FlightMapper
import joanvila.com.presentation.mapper.ScheduleMapper
import joanvila.com.presentation.mapper.ScheduledLocationMapper
import joanvila.com.presentation.routes.RoutesProcessor
import joanvila.com.presentation.routes.RoutesViewModel
import joanvila.com.presentation.schedules.SchedulesProcessor
import joanvila.com.presentation.schedules.SchedulesViewModel
import joanvila.com.presentation.splash.SplashProcessor
import joanvila.com.presentation.splash.SplashViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val presentationModule = module {

    // Splash
    viewModel { SplashViewModel(get()) }
    single { SplashProcessor(get()) }
    single { AuthenticateInteractor(get(), get(), get()) }

    // Search Airports
    viewModel { AirportsViewModel(get()) }
    single { AirportsProcessor(get(), get()) }
    single { GetAirportsInteractor(get(), get(), get()) }
    single { AirportMapper() }

    // Search Schedules
    viewModel { SchedulesViewModel(get()) }
    single { SchedulesProcessor(get(), get()) }
    single { GetSchedulesInteractor(get(), get(), get()) }
    single { ScheduleMapper(get()) }
    single { FlightMapper(get()) }
    single { ScheduledLocationMapper() }

    // Routes
    viewModel { RoutesViewModel(get()) }
    single { RoutesProcessor(get()) }
    single { GetAirportInteractor(get(), get(), get()) }
}